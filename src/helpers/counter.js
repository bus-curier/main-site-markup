module.exports = function(options) {
  const name = options.hash.name;
  const mods = options.hash.mods;
  const root = options.data.root.root;
  let cssClass = 'counter';
  let allMods = '';

  if (mods !== 'undefined' && mods ) {
    const modsList = mods.split(',');
      for (let i = 0; i < modsList.length; i++) {
        allMods = allMods + ' counter--' + modsList[i].trim();
      }
  }

  cssClass+= allMods;

   const counter = `
             <div class="${cssClass}" data-counter>
                <button type="button" class="counter__btn" data-dec>
                  <svg class="icon" width="19" height="19">
                    <use xlink:href="${root}assets/img/symbol/sprite.svg#minus">
                  </svg>
                </button>
                <input type="text" ${name ? `name="${name}"` : ``} class="counter__input" value="0" disabled="true" />
                <button type="button" class="counter__btn" data-inc>
                  <svg class="icon" width="19" height="19">
                    <use xlink:href="${root}assets/img/symbol/sprite.svg#plus">
                  </svg>
                </button>
             </div>`

  return counter;
}
